'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Clientes', {
      cpf: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.DECIMAL
      },
      nome: {
        type: Sequelize.STRING
      },
      login: {
        type: Sequelize.STRING
      },
      senha: {
        type: Sequelize.STRING
      },
      dataNasc: {
        type: Sequelize.DATE
      },
      sexo: {
        type: Sequelize.CHAR
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Clientes');
  }
};