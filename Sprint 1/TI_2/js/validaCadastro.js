function valida()
{
    let cliente ={
        nome: $('#name').val(),
        cpf:  $('#cpf').val(),
        login: $('#email').val(),
        sexo: $('#selectSexo').val(), 
        dataNasc: $('#dataNasc').val(),
        senha: $("#password").val()
    }
    $.ajax({ 
        type: 'POST', 
        contentType:'application/json',
        dataType: 'json',
        url: 'http://localhost:8080/cliente',
        data: JSON.stringify(cliente),
        success: function(answer){   
            if(answer.error === false){
                  alert("Cliente cadastro com sucesso! "+answer);
                  console.log(JSON.stringify(answer.val()));
            } 
            if(answer.error === true){
                console.log(JSON.stringify(answer.message.val()));
            }
        }
     });
}