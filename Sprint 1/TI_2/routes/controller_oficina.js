var express = require('express');
var router = express.Router();
var model  = require('../models');
/* GET todo listing. */

router.get('/', function(req, res, next) {
    model.Oficina.findAll({attributes: ['cnpj','nome']})
    .then(oficinas => res.json({
        error: false,
        data: oficinas
    }))
    .catch(error => res.json({
        error: true,
        data: [],
        message: error
    }));
}); 
/* POST todo. */
router.post('/', function (req, res, next) {
    const {
        cnpj,
        nome,
        login,
        senha
    } = req.body;
    model.Oficina.create({
        cnpj:cnpj,  
        nome: nome,
        login:login,
        senha:senha
        })
        .then(oficina => res.status(201).json({
            error: false,
            data: oficina,
            message: 'Nova Oficina Cadastrada!'
        }))
        .catch(error => res.json({
            error: true,
            data: [],
            message: error
        }));
});
/* update todo. */
router.put('/:cnpj', function(req, res, next) {
    const user_cnpj = req.params.cnpj;
    const{nome,email} = req.body;
    model.Oficina.update({
        nome: nome,
        email: email
    },{
        where: {
            cnpj: user_cnpj
        }
    })
    .then(oficina => res.status(201).json({
        error: false,
        message: 'Oficina atualizada!'
    }))
    .catch(error => res.json({
        error: true,
        message: error
    }))
});
/* GET todo listing. */
router.delete('/:cnpj', function(req, res, next) {
    const user_cnpj = req.params.cnpj;
    model.Oficina.destroy({ where:{
        cnpj: user_cnpj
    }})
    .then(status => res.status(201).json({
        error: false,
        message: "Oficina excluída"
    }))
    .catch(error => res.json({
        error: true,
        message: error
    }));
});

module.exports = router;