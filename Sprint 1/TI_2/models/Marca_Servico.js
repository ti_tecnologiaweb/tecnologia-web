/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Marca_Servico', {
    num_seq: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    id_servico: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'Servico',
        key: 'id_servico'
      }
    },
    valor: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    data_marcado: {
      type: DataTypes.DATE,
      allowNull: true
    },
    id_cliente: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      references: {
        model: 'Clientes',
        key: 'cpf'
      }
    },
    id_oficina: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      references: {
        model: 'Oficina',
        key: 'cnpj'
      }
    },
    observacao: {
      type: DataTypes.STRING(250),
      allowNull: true
    },
    realizado: {
      type: DataTypes.INTEGER(1),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'Marca_Servico'
  });
};
