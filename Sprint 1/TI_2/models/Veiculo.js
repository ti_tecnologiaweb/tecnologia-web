/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Veiculo', {
    veiculo_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    marca: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    modelo: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    placa: {
      type: DataTypes.STRING(8),
      allowNull: false
    },
    cliente_id: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      references: {
        model: 'Clientes',
        key: 'cpf'
      }
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'Veiculo'
  });
};
