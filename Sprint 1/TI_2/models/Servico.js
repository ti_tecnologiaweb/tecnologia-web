/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Servico', {
    id_servico: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    descri_servico: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    nome_servico: {
      type: DataTypes.STRING(80),
      allowNull: true
    }
  }, {
    tableName: 'Servico'
  });
};
