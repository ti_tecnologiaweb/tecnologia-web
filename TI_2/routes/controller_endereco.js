var express = require('express');
var router = express.Router();
var model  = require('../models');
/* GET todo listing. */
router.get('/enderecoCliente/:cpf', function(req, res, next) {
    const user_cpf = req.params.cpf;
    model.Endereco_Cliente.findAll({attributes: ['rua','numero','bairro','cidade','cep'],
                                    where:{
                                        cpf_cliente:user_cpf
                                    }
                                })
    .then(endereco_cliente => res.json({
        error: false,
        data: endereco_cliente
    }))
    .catch(error => res.json({
        error: true,
        data: [],
        message: error
    }));
});
router.post('/enderecoCliente/',function(req,res,next){
    const {
        bairro,
        cidade,
        rua,
        cep,
        numero,
        cpf
    } = req.body;
    model.Endereco_Cliente.create({
        cpf_cliente:cpf,  
        rua: rua,
        bairro:bairro,
        cep:cep,
        numero: numero,
        cidade: cidade
        })
        .then(endCliente => res.status(201).json({
            error: false,
            data: endCliente,
            message: 'Endereço Cliente Cadastrado!'
        }))
        .catch(error => res.json({
            error: true,
            data: [],
            message: error,
    }));
});
/* POST todo. */
router.post('/enderecoOficina/', function (req, res, next) {
    const {
        bairro,
        cidade,
        rua,
        cep,
        numero,
        cnpj
    } = req.body;
    model.Endereco_Oficina.create({
        cnpj_oficina:cnpj,  
        rua: rua,
        cidade:cidade,
        bairro:bairro,
        cep: cep,
        numero: numero
        })
        .then(enderecoOficina => res.status(201).json({
            error: false,
            data: enderecoOficina,
            message: 'Endereço Cadastrado!'
        }))
        .catch(error => res.json({
            error: true,
            data: [],
            message: error
        }));
});
/* update todo. */
router.put('/enderecoCliente/:cpf', function(req, res, next) {
    const user_cpf = req.params.cpf;
    const{bairro, cidade,rua,cep,numero} = req.body;
    model.Endereco_Cliente.update({
        bairro: bairro,
        cidade: cidade,
        rua: rua,
        cep: cep,
        numero: numero
    },{
        where: {
            cpf_cliente: user_cpf
        }
    })
    .then(cliente => res.status(201).json({
        error: false,
        message: 'Cliente atualizado!'
    }))
    .catch(error => res.json({
        error: true,
        message: error
    }))
});
/* GET todo listing.*/
router.delete('/enderecoCliente/:cpf', function(req, res, next) {
    const user_cpf = req.params.cpf;
    model.Endereco_Cliente.destroy({ where:{
        cpf_cliente: user_cpf
    }})
    .then(status => res.status(201).json({
        error: false,
        message: "Endereço de Cliente excluído"
    }))
    .catch(error => res.json({
        error: true,
        message: error
    }));
});

module.exports = router;