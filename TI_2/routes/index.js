var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/cadastroCliente',function(req,res,next){
  res.render('cadastroCliente',{title:'Express'});
});
router.get('/cadastroOficina',function(req,res,next){
  res.render('cadastroOficina',{title:'Express'});
});
router.get('/solicitarServico',function(req,res,next){
  res.render('solicitarServico',{title:'Express'});
});
router.get('/login',function(req,res,next){
  res.render('login',{title:'Express'});
});
router.get('/dadosCliente',function(req,res,next){
  res.render('dadosCliente',{title:'Express'});
});
router.get('/atualizaCliente',function(req,res,next){
  res.render('atualizaCliente',{title:'Express'});
});

module.exports = router;
