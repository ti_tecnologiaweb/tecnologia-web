var express = require('express');
var router = express.Router();
var model  = require('../models');
/* GET todo listing. */
router.get('/:cpf', function(req, res, next) {
    const user_cpf = req.params.cpf;
    model.Veiculo.findAll({attributes: ['modelo','placa','marca'],
                            where:{
                                cliente_id:user_cpf
                            }})
    .then(veiculos => res.json({
        error: false,
        data: veiculos
    }))
    .catch(error => res.json({
        error: true,
        data: [],
        message: error
    }));
});/*
router.get('/:cpf', function(req, res, next) {
    const user_cpf = req.params.cpf;
    model.Clientes.findAll({attributes: ['nome'],
                            where:{ 
                                cpf:user_cpf
                            }})
    .then(clientes => res.json({
        error: false,
        data: clientes
    }))
    .catch(error => res.json({
        error: true,
        data: [],
        message: error
    }));
});
router.post('/',function(req,res,next){
    const{
        email,pwd
        }=req.body;
    model.Clientes.findAll({
        where:{
            login:email,
            senha: pwd
        }
    })
    .then(clientes => res.status(201).json({
        error: false,
        data: clientes,
        message: "Cliente logado com sucesso!",
        flag: 0
        
    }))
    .catch(error => res.json({
        error: error,
        message: "Cliente inexistente"+res,
        flag: 1
    }));
});
/* POST todo. */
router.post('/criaVeiculo', function (req, res, next) {
    const {
        ano,
        marca,
        modelo,
        placa,
        cpf
    } = req.body;
    model.Veiculo.create({
        ano:ano,  
        marca: marca,
        modelo:modelo,
        placa:placa,
        cliente_id: cpf
        })
        .then(veiculo => res.status(201).json({
            error: false,
            data: veiculo,
            message: 'Carro adicionado ao cliente!'
        }))
        .catch(error => res.json({
            error: true,
            data: [],
            message: error
        }));
});
/* update todo. */
router.put('/:cpf', function(req, res, next) {
    const user_cpf = req.params.cpf;
    const{nome, dataNasc,email} = req.body;
    model.Clientes.update({
        nome: nome,
        dataNasc: dataNasc,
        email: email
    },{
        where: {
            cpf: user_cpf
        }
    })
    .then(cliente => res.status(201).json({
        error: false,
        message: 'Cliente atualizado!'
    }))
    .catch(error => res.json({
        error: true,
        message: error
    }))
});
/* GET todo listing. */
router.delete('/:cpf', function(req, res, next) {
    const user_cpf = req.params.cpf;
    model.Veiculo.destroy({ where:{
        cliente_id: user_cpf
    }})
    .then(status => res.status(201).json({
        error: false,
        message: "Veiculos excluídos"
    }))
    .catch(error => res.json({
        error: true,
        message: error
    }));
});

module.exports = router;