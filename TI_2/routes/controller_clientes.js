var express = require('express');
var router = express.Router();
var model  = require('../models');
/* GET todo listing. */
router.get('/', function(req, res, next) {
    model.Clientes.findAll({attributes: ['nome','cpf','dataNasc','sexo']})
    .then(clientes => res.json({
        error: false,
        data: clientes
    }))
    .catch(error => res.json({
        error: true,
        data: [],
        message: error
    }));
});
router.get('/:cpf', function(req, res, next) {
    const user_cpf = req.params.cpf;
    model.Clientes.findAll({attributes: ['nome','cpf','login'],
                            where:{ 
                                cpf:user_cpf
                            }})
    .then(clientes => res.json({
        error: false,
        data: clientes
    }))
    .catch(error => res.json({
        error: true,
        data: [],
        message: error
    }));
});
router.post('/',function(req,res,next){
    const{
        email,pwd
        }=req.body;
    model.Clientes.findAll({
        where:{
            login:email,
            senha: pwd
        }
    })
    .then(clientes => res.status(201).json({
        error: false,
        data: clientes,
        message: "Cliente logado com sucesso!",
        flag: 0
        
    }))
    .catch(error => res.json({
        error: error,
        message: "Cliente inexistente"+res,
        flag: 1
    }));
});
/* POST todo. */
router.post('/criaUsuario', function (req, res, next) {
    const {
        cpf,
        nome,
        login,
        senha,
        dataNasc,
        sexo
    } = req.body;
    model.Clientes.create({
        cpf:cpf,  
        nome: nome,
        login:login,
        senha:senha,
        dataNasc: dataNasc,
        sexo: sexo
        })
        .then(cliente => res.status(201).json({
            error: false,
            data: cliente,
            message: 'Novo Cliente Cadastrado!'
        }))
        .catch(error => res.json({
            error: true,
            data: [],
            message: error
        }));
});
/* update todo. */
router.put('/:cpf', function(req, res, next) {
    const user_cpf = req.params.cpf;
    const{nome,login} = req.body;
    model.Clientes.update({
        nome: nome,
        login: login
    },{
        where: {
            cpf: user_cpf
        }
    })
    .then(cliente => res.status(201).json({
        error: false,
        message: 'Cliente atualizado!'
    }))
    .catch(error => res.json({
        error: true,
        message: error
    }))
});
/* GET todo listing. */
router.delete('/:cpf', function(req, res, next) {
    const user_cpf = req.params.cpf;
    model.Clientes.destroy({ where:{
        cpf: user_cpf
    }})
    .then(status => res.status(201).json({
        error: false,
        message: "Cliente excluído"
    }))
    .catch(error => res.json({
        error: true,
        message: error
    }));
});

module.exports = router;