var express = require('express');
var router = express.Router();
var model  = require('../models');
/* GET todo listing. */

router.get('/', function(req, res, next) {
    model.Servico.findAll({attributes: ['nome_servico','id_servico']})
    .then(servicos => res.json({
        error: false,
        data: servicos
    }))
    .catch(error => res.json({
        error: true,
        data: [],
        message: error
    }));
}); 
/* POST todo. */
router.post('/', function (req, res, next) {
    const {
        data_marcado,
        id_cliente,
        id_oficina,
        observacao,
        id_servico,
    } = req.body;
    model.Marca_Servico.create({
        data_marcado:data_marcado,
        id_cliente:id_cliente,  
        id_oficina: id_oficina,
        observacao:observacao,
        realizado:false,
        id_servico: id_servico
        })
        .then(servico => res.status(201).json({
            error: false,
            data: servico,
            message: 'Serviço Marcado na Oficina '+id_oficina+' na data e hora:'+data_marcado
        }))
        .catch(error => res.json({
            error: true,
            data: [],
            message: error
        }));
});
/* update Remarcar horario 
router.put('/:cnpj', function(req, res, next) {
    const user_cnpj = req.params.cnpj;
    const{nome,email} = req.body;
    model.Oficina.update({
        nome: nome,
        email: email
    },{
        where: {
            cnpj: user_cnpj
        }
    })
    .then(oficina => res.status(201).json({
        error: false,
        message: 'Oficina atualizada!'
    }))
    .catch(error => res.json({
        error: true,
        message: error
    }))
});*/
/* GET Desistir do servico*/
router.delete('/:cpf', function(req, res, next) {
    const id_cliente = req.params.cpf;
    model.Marca_Servico.destroy({ where:{
        id_cliente: id_cliente
    }})
    .then(status => res.status(201).json({
        error: false,
        message: "Serviços excluídos"
    }))
    .catch(error => res.json({
        error: true,
        message: error
    }));
});

module.exports = router;