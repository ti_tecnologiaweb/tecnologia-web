/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Oficina', {
    nome: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    cnpj: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      primaryKey: true
    },
    login: {
      type: DataTypes.STRING(30),
      allowNull: false
    },
    senha: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    tableName: 'Oficina'
  });
};
