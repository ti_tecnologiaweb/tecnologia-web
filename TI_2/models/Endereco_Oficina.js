/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Endereco_Oficina', {
    endereco_id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    cnpj_oficina: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      references: {
        model: 'Oficina',
        key: 'cnpj'
      }
    },
    rua: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    numero: {
      type: DataTypes.STRING(7),
      allowNull: false
    },
    bairro: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    cidade: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    cep: {
      type: DataTypes.STRING(9),
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'Endereco_Oficina'
  });
};
