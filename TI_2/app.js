var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var engines = require('consolidate')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var clientes    = require('./routes/controller_clientes');
var oficinas    = require('./routes/controller_oficina');
var servico     = require('./routes/controller_servico');
var endereco    = require('./routes/controller_endereco');
var veiculo     = require('./routes/controller_veiculo');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/cliente',clientes);
app.use('/oficina',oficinas);
app.use('/servico',servico);
app.use('/endereco',endereco);
app.use('/veiculo',veiculo);

app.set('views', __dirname + '/views');
app.engine('html', engines.mustache);
app.set('view engine', 'html');
app.use(express.static(__dirname + '../public'));

app.use(function(req,res,next){
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Credentials',true);
    res.header('Access-Control-Allow-Methods','PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers','Content-Type');
});
app.get('/', function (req, res) {
    res.render('index', { title: 'Hey', message: 'Hello there!' })
})
app.get('/cadastroCliente', function (req, res) {
    res.render('cadastroCliente', { title: 'Hey', message: 'Hello there!' })
})
app.get('/cadastroOficina', function (req, res) {
    res.render('cadastroOficina', { title: 'Hey', message: 'Hello there!' })
})
app.get('/endereco', function (req, res) {
    res.render('endereco', { title: 'Hey', message: 'Hello there!' })
})
app.get('/login', function (req, res) {
    res.render('login', { title: 'Hey', message: 'Hello there!' })
})
app.get('/dadosCliente', function (req, res) {
    res.render('dadosCliente', { title: 'Hey', message: 'Hello there!' })
})
app.get('/atualizaCliente', function (req, res) {
    res.render('atualizaCliente', { title: 'Hey', message: 'Hello there!' })
})
app.listen(8080,function(){
    console.info('Server rodando!');
});
module.exports = app;
